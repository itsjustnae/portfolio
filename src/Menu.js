import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import "./App.css";
import About from './About';
import Contact from './Contact';
import Projects from './Projects';
import Skills from './Skills';

function Menu() {

  const title = 'itjustnae';
  const pages= ['LandingPage','About','Skills','Projects','Contact'];


  return (
    <div>
      <Navbar fixed="top" collapseOnSelect expand="lg" bg="light" variant="light">
        <Navbar.Brand href="/LandingPage">{title.toUpperCase()}</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/About">{pages[1].toUpperCase()}</Nav.Link>
            <Nav.Link href="/Skills">{pages[2].toUpperCase()}</Nav.Link>
            <Nav.Link href="/Projects">{pages[3].toUpperCase()}</Nav.Link>
            <Nav.Link href="/Contact">{pages[4].toUpperCase()}</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
   

  );
}

export default Menu;