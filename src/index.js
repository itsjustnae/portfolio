import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import About from './About';
import Contact from './Contact';
import Projects from './Projects';
import LandingPage from './LandingPage';
import Skills from './Skills';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';

ReactDOM.render(

  <Router>
    <Route exact path="/" component={LandingPage}/>
    <Route path="/About" component={About}/>
    <Route path="/Skills" component={Skills}/>
    <Route path="/Projects" component={Projects}/>
    <Route path="/Contact" component={Contact}/>
    <Route path="/LandingPage" component={LandingPage}/>
    <App/>
  </Router>
  
, document.getElementById('root'));

