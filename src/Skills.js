import React from 'react';
import {Grid, Cell} from 'react-mdl';

import './App.css';

function Skills() {
      return (
        <div style={{width: '80%', margin: 'auto'}} >     
          <Grid className="demo-grid-1">
        <Cell col={4}><h3>Skills</h3>
        <hr/>
        <ul> 
          <li>Built SPA using Responsive Design (Still Learning...)</li>
            <ul>
              <li>CSS Grid</li>
              <li>Flexbox</li>
              <li>Media Queries</li>
              <li>CSS Animations</li>
              <li>CSS Variables</li>
            </ul>
          <li>Semantic HTML</li>
          <li>Web Typography</li>
          <ul>
            <li>Google Fonts</li>
          </ul>
          <li>Modern UI Frameworks (React, Materialize, Bootstrap)</li>
          <li>REST/SOAP API</li>
          <li>Other Web Technologies (Node.js (npm) , Webpack, Babel, etc.)</li>
        </ul>
        </Cell>
    </Grid>

 
 </div>
    );
}

export default Skills;
