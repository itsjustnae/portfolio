import React from 'react';
import {Grid, Cell} from 'react-mdl';
import './App.css';

function Projects() {
  return (
    <div style={{width: '80%', margin: 'auto'}}>
    <Grid className="demo-grid-1">
    <Cell col={4}>
    <div class="card"style={{margin:'20px'}} >
      <img src="https://raw.githubusercontent.com/itsjustnae/Checklist-App/master/Checklist.JPG" alt="Avatar" style={{width:'100%'}}/>
      <div class="container">
        <h4><b><a className="links" href="https://bit.ly/2KKYL7B" alt="Checklist-App" placeholder="Checklist-App" target="_blank">Checklist App</a></b></h4> 
      </div>
    </div>
    </Cell>
    <Cell col={4}>
    <div class="card"style={{margin:'20px'}} >
      <img src="https://raw.githubusercontent.com/itsjustnae/Portfolio/master/src/Weather-app.JPG" alt="Avatar" style={{width:'100%'}}/>
      <div class="container">
        <h4><b><a className="links" href="https://bit.ly/2JeeitA" alt="" placeholder="Weather Location App" target="_blank">Weather Location App</a></b></h4> 
      </div>
    </div>
    </Cell>
    <Cell col={4}>
    <div class="card" style={{margin:'20px'}}>
      <img src="https://raw.githubusercontent.com/itsjustnae/Portfolio/master/src/Missing-image.png" alt="Avatar" style={{width:'100%'}}/>
      <div class="container">
        <h4><b><a className="links" href="/" alt="" placeholder="Coming Soon" target="_blank">Coming Soon</a></b></h4> 
      </div>
    </div>
    </Cell>

</Grid>
    </div>
  );
}

export default Projects;
