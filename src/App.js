import React from "react";
import "./App.css";
import Menu from './Menu';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';

import LandingPage from './LandingPage'
import About from './About';
import Contact from './Contact';
import Projects from './Projects';
import Skills from './Skills';

function App() {
  return (
    <div className="App">
      <Menu />
    </div>
   

  );
}

export default App;
